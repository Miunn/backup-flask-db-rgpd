# coding: utf-8

"""

Backup script for Testop Databse accross network

Perform a backup every hour

Each backup in encrypted with AES-256 algorithm

"""

from dotenv import load_dotenv

from datetime import datetime
from time import sleep

from os import getenv, mkdir
import mariadb

from Crypto.Cipher import AES

load_dotenv()


def get_db_params() -> dict:
    return {
        "host": getenv("DB_HOST"),
        "port": int(getenv("DB_PORT")),
        "user": getenv("DB_LOGIN"),
        "password": getenv("DB_PSWD"),
        "database": getenv("DB_NAME")
    }


def read_db():
    query = """select * from submits"""

    with mariadb.connect(**get_db_params()) as db:
        with db.cursor() as crsr:
            crsr.execute(query)
            r = crsr.fetchall()
    return r


def parse_db_data(data):
    csv_data = list()
    for entry in data:
        csv_entry = str()
        for elmt in entry:
            csv_entry += str(elmt) + ";"
        csv_data.append(csv_entry)
    return csv_data


def encrypt_data(data):
    with open('.key', 'rb') as keyfile:
        key = keyfile.read()

    cipher = AES.new(key, AES.MODE_EAX)
    cipherData, tag = cipher.encrypt_and_digest(data)
    nonce = cipher.nonce

    return (cipherData, tag, nonce)


def save_encrypt_data(cipherData, tag, nonce):
    path = "Saves/{dt}/".format(
        dt=datetime.now().strftime("%Y-%m-%d %Hh%Mm%Ss"))

    mkdir(path)

    with open(path+'/cipher', 'wb') as f:
        f.write(cipherData)

    with open(path+'/tag', 'wb') as f:
        f.write(tag)

    with open(path+'/nonce', 'wb') as f:
        f.write(nonce)


"""
c, t, n = encrypt_data("Oui bonjour".encode())

print(c, t, n)
save_encrypt_data(c, t, n)
"""

while True:
    print('[*] Read db')
    data = read_db()
    csv_data = parse_db_data(data)

    print('[*] Encrypt data')
    c, t, n = encrypt_data(str(csv_data).encode())

    print('[*] Save data')
    save_encrypt_data(c, t, n)

    print('[-] Sleeping until next backup')
    sleep(3600)
