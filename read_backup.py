# coding: utf-8

from Crypto.Cipher import AES


def decrypt_save(path):
    with open(path+'cipher', 'rb') as f:
        c = f.read()

    with open(path+'tag', 'rb') as f:
        t = f.read()

    with open(path+'nonce', 'rb') as f:
        n = f.read()

    with open('.key', 'rb') as f:
        key = f.read()

    decrypt = AES.new(key, AES.MODE_EAX, n)

    return decrypt.decrypt_and_verify(c, t).decode()
